const { multiplyEvenElementsByOne } = require('../steps/multiplyEvenElementsByOne');

describe('multiplyEvenElementsByOne', () => {
  it('should multiply even elements by one', () => {
    expect(multiplyEvenElementsByOne([4, 8, 9, 2, 5, 8, 4, 9])).toEqual([-4, 8, -9, 2, -5, 8, -4, 9]);
  });
});