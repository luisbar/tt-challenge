const { addOneToArray } = require('../steps/addOneToArray');

describe('addOneToArray', () => {
  it('should return an array with sum in one', () => {
    expect(addOneToArray([4, 8, 9, 2, 5, 8, 4, 8, 0, 0, 0])).toEqual([4, 8, 9, 2, 5, 8, 4, 9]);
  });
});