const { getBiggestSubArraySum } = require('../steps/getBiggestSubArraySum');

describe('getBiggestSubArraySum', () => {
  it('should return the biggest sub array sum', () => {
    expect(getBiggestSubArraySum([-4, 8, -9, 2, -5, 8, -4, 9])).toBe(13);
  });
});