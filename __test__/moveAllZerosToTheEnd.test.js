const { moveAllZerosToTheEnd } = require('../steps/moveAllZerosToTheEnd');

describe('moveAllZerosToTheEnd', () => {
  test('should move all zeroes to the end', () => {
    expect(moveAllZerosToTheEnd([4, 8, 0, 9, 2, 0, 5, 8, 0, 4, 8])).toEqual([4, 8, 9, 2, 5, 8, 4, 8, 0, 0, 0]);
  });
});