exports.addOneToArray = (array) => {
  const arrayWhithoutZeros = array.filter(element => element !== 0);
  const arrayToNumber = Number(arrayWhithoutZeros.join('')) + 1;

  return Array.from(String(arrayToNumber), Number);
};