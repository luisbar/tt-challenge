exports.moveAllZerosToTheEnd = (array) => {
  let zeros = 0;

  const arrayWhitoutZeros = array.filter(element => {
    if (element === 0) zeros++;
    return element !== 0;
  });

  return [...arrayWhitoutZeros, ...Array(zeros).fill(0)];
}