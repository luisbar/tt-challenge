exports.multiplyEvenElementsByOne = (array) => {
  return array.map((element, index) => {
    if (index % 2 === 0) {
      return element * - 1;
    }
    return element;
  });
}