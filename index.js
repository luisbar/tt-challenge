const { moveAllZerosToTheEnd } = require('./steps/moveAllZerosToTheEnd');
const { addOneToArray } = require('./steps/addOneToArray');
const { multiplyEvenElementsByOne } = require('./steps/multiplyEvenElementsByOne');
const { getBiggestSubArraySum } = require('./steps/getBiggestSubArraySum');

const array = [4, 8, 0, 9, 2, 0, 5, 8, 0, 4, 8];
let result = moveAllZerosToTheEnd(array);
result = addOneToArray(result);
result = multiplyEvenElementsByOne(result);
result = getBiggestSubArraySum(result);
console.log(result);